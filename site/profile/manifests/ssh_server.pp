class profile::ssh_server {
  package {'openssh-server':
    ensure => present,
  }
  service { 'sshd':
    ensure => 'running',
    enable => 'true',
  }
  ssh_authorized_key { 'root@master.puppet.vm':
    ensure => present,
    user   => 'root',
    type   => 'ssh-rsa',
    key    => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQC/rCSedzzBV/hLvfSysOmfSmu2dghiaIoW0+WygFsdGJ2PhO2A22IdjyD9wSXegbsVO+BIl5SIRrse8uxffx+3povM+eTO9qcw0g0IfDCbY5gDHa8yGAeqbfHQUZj17gmaGzWn2RMZMhyptujqyZ1pFb0+gNL+ngqraUkStaCP829/+gWhcSt4zKwxvztNFP73hjW+e9p8Js+OkG/9EDomJXIDWFLWMNncSNPGQ2tsoZhswRqk03u5YRce2e50Jomciz2Rk7YmE8S9H4sFPlJB0eWKa9Cwp10/sUsqd3rhoAcLv9X+Mc4nKvxSJgUd8GHjdmQ/upGONfjunzE2m/Ev',
  }  
}
